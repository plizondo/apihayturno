package com.hayturno.service.endpoints.provincia;

import com.hayturno.data.service.provincia.ProvinciaService;
import com.hayturno.entities.Provincia;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by ASUS on 22/02/2016.
 */
public class ProvinciaEndPointImpl implements ProvinciaEndPoint {

    private ProvinciaService provinciaService;

    @Autowired
    public ProvinciaEndPointImpl(ProvinciaService provinciaService) {
        this.provinciaService = provinciaService;
    }

    @Override
    public List<Provincia> getProvincias() {
        return provinciaService.getProvincias();
    }
}
