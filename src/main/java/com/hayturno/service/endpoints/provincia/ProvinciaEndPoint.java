package com.hayturno.service.endpoints.provincia;

import com.hayturno.entities.Provincia;
import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by ASUS on 22/02/2016.
 */
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface ProvinciaEndPoint {

    @GET
    @Path("/")
    @CrossOriginResourceSharing(allowAllOrigins = true)
    List<Provincia> getProvincias();

}
