package com.hayturno.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by ASUS on 22/02/2016.
 */
@Entity
@Table(name = "provincia")
@XmlRootElement(name = "provincias")
public class Provincia {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "idprovincia")
    private Integer id;

    @Column(name = "nombre")
    private String nombre;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
