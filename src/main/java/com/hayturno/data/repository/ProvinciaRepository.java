package com.hayturno.data.repository;

import com.hayturno.entities.Provincia;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ASUS on 22/02/2016.
 */
public interface ProvinciaRepository extends JpaRepository<Provincia,Integer> {
}
