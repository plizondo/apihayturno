package com.hayturno.data.service.provincia;

import com.hayturno.data.repository.ProvinciaRepository;
import com.hayturno.entities.Provincia;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.List;

/**
 * Created by ASUS on 22/02/2016.
 */
@Service
@EnableTransactionManagement
public class ProvinciaServiceImpl implements ProvinciaService {

    private ProvinciaRepository provinciaRepository;

    @Autowired
    public ProvinciaServiceImpl(ProvinciaRepository provinciaRepository) {
        this.provinciaRepository = provinciaRepository;
    }

    @Override
    public List<Provincia> getProvincias() {
        return (List<Provincia>)  provinciaRepository.findAll();
    }
}
