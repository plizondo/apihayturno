package com.hayturno.data.service.provincia;

import com.hayturno.entities.Provincia;

import java.util.List;

/**
 * Created by ASUS on 22/02/2016.
 */
public interface ProvinciaService {
    List<Provincia> getProvincias();

}
